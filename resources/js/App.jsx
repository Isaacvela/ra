import React from "react";
import { createRoot } from "react-dom/client";
import Login from "./Auth/Login";
import Navbar from "./Componentes/Navbar";
import LayoutPublic from "./layout/LayoutPublic";
import Inicio from "./Inicio";

function App() {
    return (
        <div>
            <LayoutPublic/>
            {/* <Inicio/> */}
        </div>
    );
}

if (document.getElementById('app')) {
    createRoot(document.getElementById('app')).render(<App />);
}