import "./Login.css"

export default function Login() {
    return (

        <div className="page">
            <div className="form-container">
                <center><img src="img/img.png" width={120} /></center>
                <br />
                <p className="title">Inicio de Sesión</p>
                <form className="form" action="POST">
                    <input className="input" type="email" placeholder="Ingrese su Correo" autoFocus required />
                    <input className="input" type="password" placeholder="Ingrese su Contraseña" autoFocus required />
                    <p className="page-link">
                        <span className="page-link-label">
                            ¿Olvidate tu Contraseña?
                        </span>
                    </p>
                    <button className="form-btn">Iniciar Sesión</button>
                    <p className="title2">¿No tienes una cuenta?</p>
                    <button className="form-btn">Registrarse</button>
                </form>
            </div>
        </div>
    )
}
