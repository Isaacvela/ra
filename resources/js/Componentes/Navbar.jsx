import React from "react";

export default function Navbar() {
    return (
        <div>
            <nav className="navbar navbar-expand-lg navbar-light bg-white py-3 fixed-top">
                <div className="container px-5">
                    <a className="navbar-brand" href="#">
                        <span className="fw-bolder text-primary">Nombre de la pagina</span>
                    </a>
                    <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation"><span className="navbar-toggler-icon"></span></button>
                    <div className="collapse navbar-collapse" id="navbarSupportedContent">
                        <ul className="navbar-nav ms-auto mb-2 mb-lg-0 small fw-bolder">
                            <li className="nav-item"><a className="nav-link" href="/Auth/Login">Login</a></li>
                            <li className="nav-item"><a className="nav-link" href="#">Registrase</a></li>
                            {/* <li className="nav-item"><a className="nav-link" href="#">Mensajes</a></li> */}
                        </ul>
                    </div>
                </div>
            </nav>
        </div>
    )
}